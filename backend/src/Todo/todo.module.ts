import {Module} from "@nestjs/common";

import {TodosService} from "./todo.service";
import {TodosController} from "./todo.controller";

@Module({
    controllers: [TodosController],
    providers: [TodosService]
})
export class TodoModule {

}
