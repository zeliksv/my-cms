import {useNavigate} from 'react-router-dom';
import {useMutation, useQueryClient} from 'react-query';
import dayjs from 'dayjs';

import TodoForm from '../../../components/Forms/TodoForm';
import {ITodo} from '../../../types.ts';
import {getTodoService} from '../../../api';
import {useUserContext} from '../../../context/UserContext';

const CreateTodo = () => {
    const {user} = useUserContext();
    const queryClient = useQueryClient();
    const mutation = useMutation((newTodo: ITodo) =>
        getTodoService().createTodo(newTodo), {
        onSuccess: () => {
            queryClient.invalidateQueries('todos');
            queryClient.fetchQuery('todos');
        }
    });
    const navigation = useNavigate();

    const onSubmit = (data: any) => {
        const dateValue = dayjs().format('YYYY-MM-DD HH:MM');

        const newData = {
            ...data,
            createdAt: dateValue,
            updatedAt: dateValue,
            author: `${user?.name}`,
            isFavorite: false,
            userId: user?.id
        };

        mutation.mutate(newData);
        navigation('/');
    };

    return (
        <>
            <TodoForm onSubmit={onSubmit} values={{
                title: '',
                description: '',
                isDone: false
            } as ITodo} />
        </>
    );
};

export default CreateTodo;
