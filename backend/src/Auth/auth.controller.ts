import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Post, Res
} from '@nestjs/common';
import { Response } from 'express';
import { AuthService } from './auth.service';
import { AuthDto } from './dto/auth.dto';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('signup')
  async signup(@Body() dto: AuthDto, @Res() response: Response) {
    const token = await this.authService.signUp(dto);

    response.send({token: token.access_token});
  }

  @HttpCode(HttpStatus.OK)
  @Post('signin')
  async signin(@Body() dto: AuthDto, @Res() response: Response) {
    const token = await this.authService.signIn(dto);

    response.send({token: token.access_token});
  }
}
