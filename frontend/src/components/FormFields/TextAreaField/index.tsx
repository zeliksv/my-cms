import {FC, ReactNode} from 'react';
import {Input} from 'antd';
import {TextAreaProps} from 'antd/lib/input';

import styles from '../TextField/styles.module.scss';

export interface ITextFieldProps extends TextAreaProps {
    label?: ReactNode;
    name: string;
}

const { TextArea } = Input;

const TextAreaField:FC<ITextFieldProps> = ({ label, name, ...rest }) => (
        <div className={styles.textFieldContainer}>
            {label && <label className={styles.textFieldLabel}>{label}</label>}
            <TextArea className={styles.textFieldInput} name={name} {...rest} />
        </div>
    );

export default TextAreaField;
