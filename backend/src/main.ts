import { NestFactory } from '@nestjs/core';
import {NestExpressApplication} from "@nestjs/platform-express";
import {ValidationPipe} from "@nestjs/common";
import * as dotenv from 'dotenv';
import * as cookieParser from 'cookie-parser';

import { AppModule } from './app.module';
import {DocumentBuilder, SwaggerModule} from "@nestjs/swagger";

async function bootstrap() {
  dotenv.config();
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.use(cookieParser());

 const config = new DocumentBuilder().setTitle('Todo list').build();
 const document = SwaggerModule.createDocument(app, config);

  SwaggerModule.setup('api', app, document);

  app.enableCors({
      origin: 'http://127.0.0.1:5173',
      methods: 'GET, POST, PUT, DELETE',
      credentials: true,
  });

  app.useGlobalPipes(new ValidationPipe({
    transform: true
  }));

  await app.listen(3030);
}

bootstrap();
