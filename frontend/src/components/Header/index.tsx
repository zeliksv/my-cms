import {useMemo} from 'react';
import { faker } from '@faker-js/faker';
import {Avatar, Layout, Menu} from 'antd';
import { HomeOutlined, UserOutlined, LogoutOutlined, FlagOutlined } from '@ant-design/icons';
import {Link, useLocation, useNavigate} from 'react-router-dom';
import {useTranslation} from 'react-i18next';

import {SUPPORTED_LANGUAGES} from '../../i18n.ts';
import {cleanToken} from '../../helpers/api.ts';
import {useUserContext} from '../../context/UserContext';

const {Header: AntHeader} = Layout;

const Header = () => {
    const navigate = useNavigate();
    const {user, setUser} = useUserContext();
    const {t, i18n} = useTranslation();

    const handleLogout = () => {
        cleanToken();

        if(setUser) {
            setUser(null);
        }

        navigate('/login');
    };
    const changeLang = (lang: string) => () => i18n.changeLanguage(lang);

    const links: Record<string, any> = {
        1: {
            label: <Link to='/'>{t('header.titles.home')}</Link>,
            key: '1',
            icon: <HomeOutlined />
        },
        2: {
            label:  <Avatar size={32} src={user?.avatar ?? faker.image.avatar()} onClick={e => e?.preventDefault()} />,
            key: '3',
            children: [
                {
                    label: <Link to='/profile'>{ t('header.titles.profile', {userName: user?.name})}</Link>,
                    key: '5',
                    icon: <UserOutlined />,
                },
                {
                    label: <span onClick={handleLogout}>{t('header.titles.logout')}</span>,
                    key: '4',
                    icon: <LogoutOutlined />,
                }
            ]
        },
        3: {
            label:  t('header.titles.language'),
            key: '6',
            children: SUPPORTED_LANGUAGES.map((lang, inx) => ({
                label: <span key={lang} onClick={changeLang(lang)}>{t(`languages.${lang}`)}</span>,
                key: `${7 + inx}`,
                icon: <FlagOutlined />
            }))
        }
    };
    const {pathname} = useLocation();
    const defaultSelectedKeys = useMemo(() => Object.keys(links).filter(item => links[item].path === pathname), [pathname, links]);

    return (
        <AntHeader>
            <Menu theme="dark" mode="horizontal" defaultSelectedKeys={defaultSelectedKeys} items={Object.values(links)} />
        </AntHeader>
    );
};

export default Header;
