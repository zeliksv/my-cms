import Cookies from 'js-cookie';
import dayjs from "dayjs";

import {EXPIRED_DATE_FORMAT} from "../constants";

export const getToken = () => Cookies.get('access_token');

export const getExpiredDate = () => Cookies.get('expired_date');

export const setToken = (value: string | null) => {
    if (value) {
        const expiredDate = dayjs().add(1, 'd').format(EXPIRED_DATE_FORMAT);

        Cookies.set('access_token', value);
        Cookies.set('expired_date', expiredDate)
    }
}

export const cleanToken = () => Cookies.remove('access_token');
