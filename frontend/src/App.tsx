import {ConfigProvider} from 'antd';
import {RouterProvider} from 'react-router-dom';
import {
    QueryClient,
    QueryClientProvider
} from 'react-query';

import {router} from './routes.tsx';
import {init} from './i18n.ts';
import {UserProvider} from './context/UserContext';
import {MAIN_COLOR} from "./constants/theme.ts";

await init();

const queryClient = new QueryClient();

const App = () => (
    <>
        <QueryClientProvider client={queryClient}>
            <ConfigProvider theme={{ token: { colorPrimary: MAIN_COLOR } }}>
                <UserProvider>
                    <RouterProvider router={router} />
                </UserProvider>
            </ConfigProvider>
        </QueryClientProvider>
    </>);

export default App;
