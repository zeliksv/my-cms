import {createBrowserRouter} from 'react-router-dom';

import Profile from './pages/Profile';
import Login from './pages/Login';
import Todo from './pages/Todo';
import UpdateTodo from './pages/TodoFormPage/Update';
import CreateTodo from './pages/TodoFormPage/Create';

export const router = createBrowserRouter([
    {
        path: '/',
        Component: Todo,
    },
    {
        path: '/login',
        Component: Login,
    },
    {
        path: '/profile',
        Component: Profile,
    },
    {
        path: '/todo/:id',
        Component: UpdateTodo,
    },
    {
        path: '/todo/new',
        Component: CreateTodo,
    }
]);
