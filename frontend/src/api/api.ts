import axios, {AxiosInstance, AxiosRequestConfig} from 'axios';

import {getToken} from '../helpers/api.ts';

export const apiInstance = axios.create({
  baseURL: import.meta.env.VITE_REACT_APP_API_URL,
  headers: {
    'Content-Type': 'application/json',
  },
  withCredentials: true
});

apiInstance.interceptors.request.use(config => {
  const token = getToken();

  if(token) {
    config.headers.Authorization = `Bearer ${  getToken()}`;
  }

  return config;
});

apiInstance.interceptors.response.use(
    response => response,
        error => Promise.reject(error));

let baseApiInstance: AxiosInstance;

export const api = <T>(
  config: AxiosRequestConfig,
  options?: AxiosRequestConfig,
): Promise<T> => {
  if(!baseApiInstance) {
    baseApiInstance = apiInstance;
  }

  return baseApiInstance({
    ...config,
    ...options,
  }).then(r => r.data);
};
