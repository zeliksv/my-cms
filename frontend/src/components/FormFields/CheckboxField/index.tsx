import React, { ReactNode } from 'react';
import { Checkbox, CheckboxProps } from 'antd';

import styles from './styles.module.scss';

interface CheckboxFieldProps extends CheckboxProps {
    label?: ReactNode;
    name: string;
}

const CheckboxField: React.FC<CheckboxFieldProps> = ({ label, name, value, ...rest }) => (
        <div className={styles.checkboxFieldContainer}>
            <Checkbox name={name} checked={value} {...rest} />
            {label && <label className={styles.checkboxFieldLabel}>{label}</label>}
        </div>
    );

export default CheckboxField;
