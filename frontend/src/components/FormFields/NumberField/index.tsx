import React, { ReactNode } from 'react';
import { InputNumber, InputNumberProps } from 'antd';
import styles from './styles.module.scss';

interface NumberFieldProps extends InputNumberProps {
    label?: ReactNode;
    name: string;
}

const NumberField: React.FC<NumberFieldProps> = ({ label, name, ...rest }) => (
        <div className={styles.numberFieldContainer}>
            {label && <label className={styles.numberFieldLabel}>{label}</label>}
            <InputNumber className={styles.numberFieldInput} name={name} {...rest} />
        </div>
    );

export default NumberField;
