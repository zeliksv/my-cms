import {Controller, useForm} from 'react-hook-form';
import {useTranslation} from 'react-i18next';
import {Button, Form} from 'antd';
import {MailOutlined} from '@ant-design/icons';
import {useMutation} from 'react-query';

import TextField from '../../../components/FormFields/TextField';

import styles from './styles.module.scss';
import {IAuth} from '../../../types.ts';
import {getUserService} from '../../../api';
import {FC} from "react";

type IProps = {
    handleUserData: () => void;
}

const SignUp:FC<IProps> = ({handleUserData}) => {
    const { handleSubmit, control } = useForm();
    const {t} = useTranslation();
    const mutation = useMutation((authData: IAuth) =>
        getUserService().createUser(authData), {
        onSuccess: handleUserData
    });

    const onSubmit = (data: any) => {
        if(data.confirmPassword !== data.password) {
            /* eslint no-console: "off" */
            console.error('Passwords are not match');
        } else {
            mutation.mutate(data);
        }
    };

    return (
        <div className={styles.formWrapper}>
            <h3>{t('auth.form-title.registration')}</h3>
            <Form onFinish={handleSubmit(onSubmit)}>
                <Controller
                    name="name"
                    control={control}
                    render={({ field }) => (
                        <TextField {...field} label={t('form.fields.name')} />
                    )}
                />
                <Controller
                    name="email"
                    control={control}
                    rules={{ required: true, pattern: /^\S+@\S+$/i }}
                    render={({ field }) => (
                        <TextField {...field} prefix={<MailOutlined />} label={t('form.fields.email')} placeholder="Email" />
                    )}
                />
                <Controller
                    name="password"
                    control={control}
                    render={({ field }) => (
                        <TextField {...field} label={t('form.fields.password')}  type="password" />
                    )}
                />
                <Controller
                    name="confirmPassword"
                    control={control}
                    render={({ field }) => (
                        <TextField {...field} label={t('form.fields.confirm-password')} type="password" />
                    )}
                />
                <Button type="primary" htmlType="submit">
                    {t('form.submitBtn', {item: 'User'})}
                </Button>
            </Form>
        </div>
    );
};

export default SignUp;
