import {useNavigate, useParams} from 'react-router-dom';
import {Skeleton} from 'antd';
import {useMutation, useQuery, useQueryClient} from 'react-query';

import TodoForm from '../../../components/Forms/TodoForm';
import {ITodo} from '../../../types.ts';
import {getTodoService} from '../../../api';

const UpdateTodo = () => {
    const {id} = useParams();
    const navigation = useNavigate();
    const {isLoading, data} = useQuery<ITodo | null>('todo', () => getTodoService().getTodoById(id), {
        enabled: Boolean(id),
    });
    const queryClient = useQueryClient();
    const mutation = useMutation((updatedTodo: ITodo) =>
        getTodoService().updateTodo(updatedTodo), {
        onSuccess: () => {
            queryClient.invalidateQueries('todos');
            queryClient.fetchQuery('todos');
        }
    });

    const onSubmit = (data: ITodo) => {
        mutation.mutate(data);

        navigation('/');
    };

    if(!data && isLoading) {
       return  <Skeleton />;
    }

    return (
        <>
            <TodoForm onSubmit={onSubmit} values={data as ITodo} />
        </>
    );
};

export default UpdateTodo;
