import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import Backend from 'i18next-http-backend';

export const SUPPORTED_LANGUAGES = ['en', 'ua'];

export const init = async () => {
    const initialLanguage = SUPPORTED_LANGUAGES[0];

      return  await i18n
            .use(Backend)
            .use(initReactI18next)
            .init({
                lng: initialLanguage,
                backend: {
                    loadPath: '/locales/{{lng}}.json'
                },
                fallbackLng: initialLanguage,
                supportedLngs: SUPPORTED_LANGUAGES,
                interpolation: {
                    escapeValue: false,
                },
            });
};
