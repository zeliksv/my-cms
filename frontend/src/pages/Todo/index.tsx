import {Button} from 'antd';
import {useNavigate} from 'react-router-dom';
import {useTranslation} from 'react-i18next';

import Layout from '../../components/Layout';
import TodosList from '../../components/TodosList';

import styles from './styles.module.scss';

const Todo = () => {
    const navigate = useNavigate();
    const {t} = useTranslation();

    return (
        <Layout>
            <TodosList />
            <div className={styles.stickyButton}>
                <Button type="primary" size="large"  onClick={() => navigate('/todo/new')}>
                    {t('form.createNewButton', {testKey: 'todo'})}
                </Button>
            </div>
        </Layout>
    );
};

export default Todo;
