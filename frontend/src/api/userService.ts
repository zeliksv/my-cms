import {ITokenResponse, IUser} from '../types.ts';
import {handleError} from '../helpers/errors.ts';
import {getToken, setToken} from '../helpers/api.ts';
import {api} from './api.ts';
import {METHOD} from '../constants/enums.ts';

export class UserService {
    public createUser = async (user: Partial<IUser>) => {
        try {
            const response = await api<ITokenResponse>(
                {
                    url: 'auth/signup',
                    method: METHOD.POST,
                    data: user
                }
            );

            setToken(response.token);
        } catch (error) {
            handleError(error);

            throw error;
        }
    };

    public updateUser = async (user: Partial<IUser>) =>  {
        try {
            return await api<IUser | null>(
                {
                    url: `/user/${user.id}`,
                    method: METHOD.PUT,
                    data: user
                }
            );
        } catch (error) {
            handleError(error);

            throw error;
        }
    };

    public login = async (data: {email: string; password: string}) =>  {
        try {
            const response = await api<ITokenResponse>(
                {
                    url: 'auth/signin',
                    method: METHOD.POST,
                    data
                }
            );

            setToken(response.token);


        } catch (error) {
            handleError(error);

            throw error;
        }
    };

    public getMe = async () =>  {
        try {
            const token = getToken();

            if(!token) {
                return null;
            }

            return await api<IUser | null>(
                {
                    url: '/user/me',
                    method: METHOD.GET
                }
            );
        } catch (error) {
            handleError(error);

            throw error;
        }
    };
}

let userServiceInstance: UserService;

export function getUserService(): UserService {
    if (userServiceInstance) {
        return userServiceInstance;
    }

    return (userServiceInstance = new UserService());
}
