import {useTranslation} from 'react-i18next';
import {Tabs} from 'antd';
import {useQueryClient} from "react-query";
import {useNavigate} from "react-router-dom";

import SignUp from './SignUp';
import SignIn from './SignIn';
import styles from './styles.module.scss';
import {IUser} from "../../types.ts";

const Login = () => {
    const {t} = useTranslation();
    const navigate = useNavigate();
    const queryClient = useQueryClient();

    const handleUserData = () => {
            queryClient.invalidateQueries('user');
            queryClient.fetchQuery<IUser>('user');
            navigate('/');
    }

    const tabsContent = [
        {
            key: '1',
            label:  t('auth.tabs-titles.sign-in'),
            children: <SignIn handleUserData={handleUserData} />
        },
        {
            key: '2',
            label: t('auth.tabs-titles.sign-up'),
            children: <SignUp handleUserData={handleUserData} />
        }
    ];

    return (
        <div className={styles.loginWrapper}>
            <Tabs
                type="card"
                items={tabsContent.map(content => ({
                        label: content.label,
                        key: content.key,
                        children: content.children,
                    }))}
            />
        </div>
    );
};

export default Login;
