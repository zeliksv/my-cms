import {Layout as AntLayout} from 'antd';
import {FC, ReactNode, useEffect} from 'react';
import {useNavigate} from 'react-router-dom';
import dayjs from "dayjs";

import Header from '../Header';
import {getExpiredDate} from "../../helpers/api.ts";

import styles from './styles.module.scss';

interface ILayoutProps {
    children?: ReactNode;
}

const { Content, Footer } = AntLayout;

const Layout:FC<ILayoutProps> = ({children}) => {
    const navigation = useNavigate();

    useEffect(() => {
        const expiredDate = getExpiredDate();

        if(!expiredDate || dayjs().isAfter(expiredDate)) {
            navigation('/login');
        }

    },[]);

    return (
        <AntLayout>
                <Header />
                <Content className={styles.content}>
                    {children}
                </Content>
                <Footer style={{ textAlign: 'center' }}>Ant Design ©2023 Created by You</Footer>
        </AntLayout>
    );
};

export default Layout;
