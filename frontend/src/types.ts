
export interface IUser {
    id: string;
    avatar: string;
    name: string;
    firstName: string;
    lastName: string;
    email: string;
    lastVisit: string;
    age: number;
    isAdvanced: boolean;
}

export interface ITodo {
    id: string;
    title: string;
    description: string;
    author: string;
    date: string;
    isDone: boolean;
    userId: string;
}

export interface IAppState {
    userData: {
        user: IUser | null;
        error: any;
        loading: boolean;
    };
}

export interface ITokenResponse {
    token: string | null;
}

export interface IAuth {
    email: string;
    password: string;
}
