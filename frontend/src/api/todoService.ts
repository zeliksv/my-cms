import {ITodo} from '../types.ts';
import {METHOD} from '../constants/enums.ts';
import {handleError} from '../helpers/errors.ts';
import {api} from './api.ts';

export class TodoService {
   public createTodo = async (todo: ITodo) => {
        try {
            return await api<ITodo>(
                {
                    url: '/todos/new',
                    method: METHOD.POST,
                    data: todo
                }
            );
        } catch (error) {
            handleError(error);

            throw error;
        }
    };

    public deleteTodo = async (id: string) => {
        try {

            return await api<ITodo>(
                {
                    url: `/todos/${id}`,
                    method: METHOD.DELETE
                }
            );
        } catch (error) {
            handleError(error);

            throw error;
        }
    };

    public getAllTodos = async (userId: string | undefined): Promise<ITodo[]> => {
        try {
            if(!userId) {
                return Promise.resolve([]);
            }

            return await api<ITodo[]>(
                {
                    url: `/todos/by-user-id/${userId}`,
                    method: METHOD.GET
                }
            );
        } catch (error) {
            handleError(error);

            throw error;
        }
    };

    public getTodoById = async (id: string | undefined) => {
        try {
            if(!id) {
                return null;
            }

            return await api<ITodo | null>(
                {
                    url: `/todos/by-todo-id/${id}`,
                    method: METHOD.GET
                }
            );
        } catch (error) {
            handleError(error);

            throw error;
        }
    };

    public updateTodo = async (todo: Partial<ITodo>) => {
        try {
            return await api<ITodo>(
                {
                    url: `/todos/${todo.id}`,
                    method: METHOD.PUT,
                    data: todo
                }
            );
        } catch (error) {
            handleError(error);

            throw error;
        }
    };
}

let todoServiceInstance: TodoService;

export function getTodoService(): TodoService {
    if (todoServiceInstance) {
        return todoServiceInstance;
    }

    return (todoServiceInstance = new TodoService());
}
