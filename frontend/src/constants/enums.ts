export enum METHOD {
    POST = 'post',
    GET = 'get',
    PUT = 'put',
    DELETE = 'delete'
}
