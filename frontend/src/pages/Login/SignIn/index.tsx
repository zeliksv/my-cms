import {Button, Form, Spin} from 'antd';
import {Controller, useForm} from 'react-hook-form';
import {MailOutlined} from '@ant-design/icons';
import {useTranslation} from 'react-i18next';
import {useMutation} from 'react-query';

import TextField from '../../../components/FormFields/TextField';

import {IAuth} from '../../../types.ts';
import {getUserService} from '../../../api';
import {FC} from "react";

type IProps = {
    handleUserData: () => void;
}

const SignIn:FC<IProps> = ({handleUserData}) => {
    const { handleSubmit, control } = useForm();
    const {t} = useTranslation();
    const {mutate, isLoading} = useMutation((authData: IAuth) =>
        getUserService().login(authData), {
        onSuccess: handleUserData
    });

    const onSubmit = (data: any) => {
        mutate(data);
    };

    return (
        <>
            <h2>{t('auth.form-title.login')}</h2>
            <Form onFinish={handleSubmit(onSubmit)}>
                <Controller
                    name="email"
                    control={control}
                    rules={{ required: true, pattern: /^\S+@\S+$/i }}
                    render={({ field }) => (
                        <TextField {...field} prefix={<MailOutlined />} label={t('form.fields.email')} placeholder="Email" />
                    )}
                />
                <Controller
                    name="password"
                    control={control}
                    render={({ field }) => (
                        <TextField {...field} label={t('form.fields.password')} type="password" />
                    )}
                />
                {isLoading ? <Spin /> : ( <Button type="primary" htmlType="submit">
                    {t('auth.form-title.login')}
                </Button>)}
            </Form>
        </>
    );
};

export default SignIn;
