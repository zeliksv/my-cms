import {nanoid} from "nanoid";
import {Injectable} from "@nestjs/common";
import {Todo} from "@prisma/client";

import {PrismaService} from "../prisma/prisma.service";

@Injectable()
export class TodosService {
    constructor(private prisma: PrismaService) {}

    async createTodo(todo: Todo) {
        try {
            return await this.prisma.todo.create({
                data: {...todo, id: nanoid()},
            });
        } catch(error) {
            console.error(error);
        }
    }
    async updateTodo(id: string, todo: Todo) {
        try {
            return await this.prisma.todo.update({
                where: {id},
                data: todo,
            });
        } catch (error) {
            console.error(error);
        }
    }
    async deleteTodo(id: string) {
        try {
            return await this.prisma.todo.delete({
                where: { id }
            });
        } catch (error) {
            console.error(error);
        }
    }
    async getAllTodos(userId: string) {
        try {
            return await this.prisma.todo.findMany({
                where: {
                    userId
                }
            });
        } catch (error) {
            throw new Error('Запрошенной задачи не существует');
        }
    }
    async getTodoById(id: string) {
        try {
            return await this.prisma.todo.findFirst({
                where: {id}
            });
        } catch (error) {
            throw new Error('Запрошенной задачи не существует');
        }
    }
}
