import {FC, useEffect} from 'react';
import {Controller, useForm} from 'react-hook-form';
import {useTranslation} from 'react-i18next';
import {Button, Form} from 'antd';

import TextField from '../../FormFields/TextField';
import TextAreaField from '../../FormFields/TextAreaField';
import CheckboxField from '../../FormFields/CheckboxField';

import styles from './styles.module.scss';
import {ITodo} from '../../../types.ts';
import Layout from '../../Layout';

export interface IFilmsFormProps {
    onSubmit: (data: ITodo) => void;
    values: ITodo | null;
}

const TodoForm:FC<IFilmsFormProps> = ({onSubmit, values}) => {
    const {t} = useTranslation();
    const { handleSubmit, control, reset } = useForm({
        defaultValues: values ?? {}
    });

    useEffect(() => {
        if(values) {
            reset(values);
        }
    }, [values]);

    return (
        <Layout>
            <div className={styles.formWrapper}>
            <Form onFinish={handleSubmit(onSubmit)}>
                <div className={styles.textFields}>
                    <Controller
                        name="title"
                        control={control}
                        render={({ field }) => (
                            <TextField {...field} label={t('todo.fields.title')} />
                        )}
                    />
                    <Controller
                        name="description"
                        control={control}
                        render={({ field }) => (
                            <TextAreaField {...field} label={t('todo.fields.description')} />
                        )}
                    />

                    <Controller
                        name="isDone"
                        control={control}
                        render={({ field }) => (
                            <CheckboxField {...field} label={t('todo.fields.done')} />
                        )}
                    />

                    <Button type="primary" htmlType="submit">
                        {t('form.submitBtn', {item: 'todo'})}
                    </Button>
                </div>
            </Form>
            </div>
        </Layout>
    );
};

export default TodoForm;
