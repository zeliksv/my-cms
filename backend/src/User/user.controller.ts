import {Body, Controller, Get, Param, Put, UseGuards} from '@nestjs/common';
import {User} from "@prisma/client";
import {ApiOkResponse} from "@nestjs/swagger";

import {GetUser} from "../Auth/decorator/get-user.decorator";
import {UserService} from "./user.service";
import {JwtGuard} from "../Auth/guard/jwt.guard";
import {CreateUserDto} from "./user.dto";

@UseGuards(JwtGuard)
@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) {}

    @Get('me')
    @ApiOkResponse({
        type: CreateUserDto,

    })
    getMe(@GetUser() user: User) {
        return user;
    }

    @Get(':id')
    async getById(@Param('id') id: string): Promise<User> {
        return await this.userService.getById(id);
    }

    @Put(':id')
    async update(@Param('id') id: string, @Body() user: User) {
        return await this.userService.update(id, user);
    }
}
