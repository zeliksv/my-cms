import {Input, InputProps} from 'antd';
import {FC, ReactNode} from 'react';

import styles from './styles.module.scss';

export interface ITextFieldProps extends InputProps {
    label?: ReactNode;
    name: string;
}

const TextField:FC<ITextFieldProps> = ({ label, name, ...rest }) => (
        <div className={styles.textFieldContainer}>
            {label && <label className={styles.textFieldLabel}>{label}</label>}
            <Input className={styles.textFieldInput} name={name} {...rest} />
        </div>
    );

export default TextField;
