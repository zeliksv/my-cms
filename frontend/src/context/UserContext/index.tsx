import {createContext, FC, ReactNode, useContext, useEffect, useReducer} from 'react';
import {useQuery} from 'react-query';

import {IUser} from '../../types.ts';
import {getUserService} from '../../api';


interface IUserContext {
    user?: IUser | null;
    setUser?: (data: IUser | null) => void ;
}
interface UserProviderProps {
    children: ReactNode;
}

const user:IUser | null | undefined = null;

export const UserContext = createContext<IUserContext>({user});

export const useUserContext = () => useContext(UserContext);

const userReducer = (state: IUser | null | undefined, action: {type: string; payload: IUser | null}) => {
 switch (action.type) {
        case 'SET_USER':
            return action.payload;
        default:
            return state;
    }
};

export const UserProvider:FC<UserProviderProps> = ({ children }) => {
    const [state, dispatch] = useReducer(userReducer, user);
    const {data, isLoading} = useQuery('user', () => getUserService().getMe());

    useEffect(() => {
        if(!isLoading && data && setUser) {
            setUser(data);
        }
    }, [data, isLoading]);

    const setUser = (data: IUser | null) => {
        dispatch({ type: 'SET_USER', payload: data });
    };

    return (
        <UserContext.Provider value={{ user: state, setUser }}>
            {children}
        </UserContext.Provider>
    );
};
