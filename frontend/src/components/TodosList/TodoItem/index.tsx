import {FC} from 'react';
import {Button, Popconfirm} from 'antd';
import {DeleteOutlined, EditOutlined} from '@ant-design/icons';
import {useNavigate} from 'react-router-dom';
import {useMutation, useQueryClient} from 'react-query';

import {ITodo} from '../../../types.ts';

import styles from './styles.module.scss';
import {getTodoService} from '../../../api';

interface IProps {
    todo: ITodo;
}

const TodoItem:FC<IProps> = ({todo}) => {
    const navigation = useNavigate();
    const queryClient = useQueryClient();
    const {mutate} = useMutation((id: string) =>
        getTodoService().deleteTodo(id),
        {
            onSuccess: () => {
                queryClient.invalidateQueries('todos');
                queryClient.fetchQuery('todos');
            }
        }
    );

    const handleEditTodo = () => navigation(`/todo/${todo.id}`);

    const handleCancel = (event: any) => {
        event.stopPropagation();
    };

    const removeTodo = () => {
        mutate(todo.id);
    };

    return (
        <div key={todo.id} className={styles.todoItem}>
            <h2>{todo.title}</h2>
            <p>{todo.description}</p>
            <div>
                <p>{todo.author}</p>
                <p>{todo.date}</p>
            </div>
            <Popconfirm
                title="Delete the todo"
                description="Are you sure to delete this todo?"
                onConfirm={removeTodo}
                onCancel={handleCancel}
            >
                <Button type="primary" className={styles.removeBtn} icon={<DeleteOutlined />} onClick={handleCancel} />
            </Popconfirm>
                <Button type="primary" className={styles.editBtn} icon={<EditOutlined />} onClick={handleEditTodo} />
        </div>
    );
};

export default TodoItem;
