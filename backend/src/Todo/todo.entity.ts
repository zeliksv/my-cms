export interface ITodo {
    id: string;
    title: string;
    userId: string;
    description: string;
    author: string;
    createdAt: string;
    updatedAt: string;
    isDone: boolean;
    isFavorite: boolean;
}
