import { Injectable } from '@nestjs/common';

import { PrismaService } from '../prisma/prisma.service';
import {User} from "@prisma/client";

@Injectable()
export class UserService {
    constructor(private prisma: PrismaService) {}

    async getById(id: string): Promise<User | null> {
        const userResult = await  this.prisma.user.findUnique({
            where: {
                id
            },
        });

        delete userResult.hash;

        return userResult;
    }

    async update(id: string, user: User): Promise<User | null> {
        try {
            const userResult = await this.prisma.user.update({
                where: {
                    id,
                },
                data: user,
            });

            delete userResult.hash;

            return userResult;

        } catch(error) {
            console.error(error);
        }
    }
}
