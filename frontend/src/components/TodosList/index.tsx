import {useQuery} from 'react-query';
import { Skeleton } from 'antd';

import TodoItem from './TodoItem';
import {getTodoService} from '../../api';

import styles from './styles.module.scss';
import {ITodo} from '../../types.ts';
import {useUserContext} from '../../context/UserContext';

const TodosList = () => {
    const {user} = useUserContext();
    const {isLoading, data = []} = useQuery<ITodo[]>('todos', () => getTodoService().getAllTodos(user?.id), {
        enabled: Boolean(user?.id),
    });

    if(isLoading) {
        return <Skeleton />;
    }

    return (
        <div className={styles.todoList}>
            {data.map(todo => (<TodoItem key={todo.id} todo={todo} />))}
        </div>
    );
};

export default TodosList;
