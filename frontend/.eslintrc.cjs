module.exports = {
  root: true,
  env: { browser: true, es2020: true },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react-hooks/recommended',
  ],
  ignorePatterns: ['dist', '.eslintrc.cjs', 'vite-env.d.ts', 'vite.config.ts'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: "./tsconfig.json"
  },
  plugins: ['react-refresh', "@typescript-eslint", "eslint-plugin-import"],
  rules: {
    'react-refresh/only-export-components': [
      'warn',
      { allowConstantExport: true },
    ],
      '@typescript-eslint/array-type': [
        'error',
        {
          default: 'array'
        }
      ],
      '@typescript-eslint/consistent-type-assertions': 'error',
      '@typescript-eslint/consistent-type-definitions': 'error',
      '@typescript-eslint/dot-notation': 'error',
      '@typescript-eslint/member-delimiter-style': [
        'error',
        {
          multiline: {
            delimiter: 'semi',
            requireLast: true
          },
          singleline: {
            delimiter: 'semi',
            requireLast: false
          }
        }
      ],
      '@typescript-eslint/member-ordering': 'error',
      '@typescript-eslint/naming-convention': [
        'error',
        {
          selector: 'variable',
          format: ['camelCase', 'UPPER_CASE'],
          types: ['boolean', 'string', 'number', 'array']
        },
        {
          selector: 'variable',
          format: ['camelCase', 'PascalCase', 'UPPER_CASE'], // This covers standard funcs, React.FC pattern and action pattern.
          types: ['function']
        },
        {
          selector: 'function',
          format: ['camelCase']
        },
        {
          selector: ['enum', 'interface'],
          format: ['PascalCase']
        }
      ],
      '@typescript-eslint/no-empty-function': 'error',
      '@typescript-eslint/no-empty-interface': [
        'error',
        {
          allowSingleExtends: true
        }
      ],
      '@typescript-eslint/no-for-in-array': 'error',
      '@typescript-eslint/no-misused-new': 'error',
      '@typescript-eslint/no-namespace': 'error',
      '@typescript-eslint/no-unnecessary-type-assertion': 'error',
      '@typescript-eslint/no-unused-expressions': 'error',
      '@typescript-eslint/no-unused-vars': 'off',
      '@typescript-eslint/prefer-for-of': 'error',
      '@typescript-eslint/quotes': [
        'error',
        'single',
        {
          avoidEscape: true
        }
      ],
      '@typescript-eslint/triple-slash-reference': 'off',
      '@typescript-eslint/unified-signatures': 'error',
      '@typescript-eslint/typedef': [
        'error',
        {
          arrowParameter: false,
          parameter: true,
          propertyDeclaration: true,
          memberVariableDeclaration: true,
          variableDeclarationIgnoreFunction: true
        }
      ],
    "react-hooks/rules-of-hooks": "off",
      '@typescript-eslint/no-non-null-assertion': 'off',
      '@typescript-eslint/no-explicit-any': 'off',
      '@typescript-eslint/ban-types': [
        'error',
        {
          types: {
            Function: 'Avoid using Function type. Use function signature instead - e.g., addTwo(numToAdd: number): number'
          }
        }
      ],
      'arrow-body-style': ['error', 'as-needed'],
      'arrow-parens': ['error', 'as-needed'],
      complexity: [
        'error',
        {
          max: 40
        }
      ],
      'constructor-super': 'error',
      curly: 'error',
      'default-case': 'error',
      'eol-last': 'error',
      eqeqeq: ['error', 'always'],
      'id-blacklist': ['error', 'any', 'Number', 'number', 'String', 'string', 'Boolean', 'boolean', 'Undefined', 'undefined'],
      'id-match': 'error',
      'linebreak-style': ['error', 'unix'],
      'max-classes-per-file': ['error', 5],
      'new-parens': 'error',
      'no-bitwise': 'error',
      'no-caller': 'error',
      'no-cond-assign': 'error',
      'no-console': 'error',
      'no-constant-condition': 'error',
      'no-control-regex': 'error',
      'no-debugger': 'error',
      'no-empty': 'error',
      'no-eval': 'error',
      'no-fallthrough': 'error',
      'no-invalid-regexp': 'error',
      'no-irregular-whitespace': 'error',
      'no-multiple-empty-lines': 'error',
      'no-new-wrappers': 'error',
      'no-shadow': 'off',
      'no-redeclare': 'off',
      'no-sparse-arrays': 'error',
      'no-template-curly-in-string': 'error',
      'no-throw-literal': 'error',
      'no-underscore-dangle': 'error',
      'no-unsafe-finally': 'error',
      'no-unused-labels': 'error',
      'no-var': 'error',
      'object-shorthand': 'error',
      'one-var': ['error', 'never'],
      'padding-line-between-statements': [
        'error',
        {
          blankLine: 'always',
          prev: '*',
          next: 'return'
        }
      ],
      'prefer-const': 'error',
      'prefer-template': 'error',
      'quote-props': ['error', 'as-needed'],
      radix: 'error',
      'react/jsx-curly-spacing': 'off',
      'react/jsx-wrap-multilines': 'off',
      'react/prop-types': 'off',
      'space-before-function-paren': [
        'error',
        {
          anonymous: 'never',
          named: 'never',
          asyncArrow: 'always'
        }
      ],
      'spaced-comment': [
        'error',
        'always',
        {
          markers: ['/']
        }
      ],
      'space-infix-ops': 'error',
      'use-isnan': 'error',
      'semi': 'error',
      'brace-style': 'error',
      'no-duplicate-case': 'error',
      'no-ex-assign': 'error',
      'no-extra-semi': 'error',
      'no-inner-declarations': 'error',
      'no-extra-parens': ['error', 'functions'],
      'import/no-duplicates': 'error',
      'func-style': ['error', 'declaration', {allowArrowFunctions: true}],
      'import/no-named-as-default': 'off',
      'react-hooks/exhaustive-deps': 'off'
    }
}
