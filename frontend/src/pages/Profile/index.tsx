import {useMutation, useQueryClient} from 'react-query';
import {Form, Button, Avatar} from 'antd';
import { UserOutlined, MailOutlined } from '@ant-design/icons';
import {useTranslation} from 'react-i18next';
import { useForm, Controller } from 'react-hook-form';
import {faker} from "@faker-js/faker";

import Layout from '../../components/Layout';
import TextField from '../../components/FormFields/TextField';
import NumberField from '../../components/FormFields/NumberField';
import CheckboxField from '../../components/FormFields/CheckboxField';

import styles from './styles.module.scss';
import {IUser} from '../../types.ts';
import {getUserService} from '../../api';
import {useUserContext} from '../../context/UserContext';

const Profile = () => {
    const {user, setUser} = useUserContext();
    const queryClient = useQueryClient();
    const mutation = useMutation((updatedUser: IUser) =>
        getUserService().updateUser(updatedUser),
        {
            onSuccess: () => {
                queryClient.invalidateQueries('user');
                queryClient.fetchQuery<IUser>('user').then(res =>
                    setUser && setUser(res));
            }
        }
    );
    const {t} = useTranslation();
    const { handleSubmit, control } = useForm({
        defaultValues: {
            avatar: faker.image.avatar(),
            name: '',
            firstName: '',
            lastName: '',
            email: '',
            isAdvanced: false,
            age: 0
        },
        values: user as IUser
    });

    const onSubmit = (data: any) => {
        mutation.mutate(data);
    };

    return (
        <Layout>
            <div className={styles.formWrapper}>
                <h2>{t('profile.title')}</h2>
                <Form onSubmitCapture={handleSubmit(onSubmit)}>
                        <Controller
                            name="avatar"
                            control={control}
                            render={({ field }) => (
                                <Avatar
                                    size="large"
                                    src={field.value ?? faker.image.avatar()}
                                />
                            )}
                        />
                        <Controller
                            name="name"
                            control={control}
                            rules={{ required: true }}
                            render={({ field }) => (
                                <TextField {...field} prefix={<UserOutlined />} label={t('form.fields.name')} placeholder="Name" />
                            )}
                        />
                        <Controller
                            name="firstName"
                            control={control}
                            render={({ field }) => (
                                <TextField {...field} prefix={<UserOutlined />} label={t('form.fields.first-name')} placeholder="First Name" />
                            )}
                        />
                        <Controller
                            name="lastName"
                            control={control}
                            render={({ field }) => (
                                <TextField {...field} prefix={<UserOutlined />} label={t('form.fields.last-name')} placeholder="Last Name" />
                            )}
                        />
                        <Controller
                            name="email"
                            control={control}
                            rules={{ required: true, pattern: /^\S+@\S+$/i }}
                            render={({ field }) => (
                                <TextField {...field} prefix={<MailOutlined />} label={t('form.fields.email')} placeholder="Email" />
                            )}
                        />
                        <Controller
                            name="isAdvanced"
                            control={control}
                            render={({ field }) => (
                                <CheckboxField {...field} disabled label="Advanced user" />
                            )}
                        />
                        <Controller
                            name="age"
                            control={control}
                            render={({ field }) => (
                                <NumberField {...field} prefix={<UserOutlined />} label={t('form.fields.age')} placeholder="Age" />
                            )}
                        />
                        <Button type="primary" htmlType="submit">
                            {t('form.submitBtn', {item: 'Profile'})}
                        </Button>
                </Form>
            </div>
        </Layout>
    );
};

export default Profile;
