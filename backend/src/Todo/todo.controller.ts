import {Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, UseGuards} from "@nestjs/common";

import {TodosService} from "./todo.service";
import {ITodo} from "./todo.entity";
import {JwtGuard} from "../Auth/guard/jwt.guard";

@UseGuards(JwtGuard)
@Controller('todos')
export class TodosController {
constructor(private readonly todoService: TodosService) {}

    @Post('new')
    async createTodoItem(@Body() todo: ITodo) {
        try {
            return await this.todoService.createTodo(todo);
        } catch (error) {
            console.error(error);
        }
    }

    @Put(':id')
    async updateTodoItem(@Param('id') id: string, @Body() todo: ITodo) {
        try {
            return await this.todoService.updateTodo(id, todo);
        } catch (error) {
            console.error(error);
        }
    }

    @Delete(':id')
    async removeTodoItem(@Param('id') id: string) {
        try {
            return await this.todoService.deleteTodo(id);
        } catch (error) {
            console.error(error);
        }
    }

    @Get('/by-todo-id/:id')
    async getTodoItemById(@Param('id') id: string) {
        try {
            return await this.todoService.getTodoById(id);

        } catch (error) {
            throw new Error('Запрошенной задачи не getTodoItemById');
        }
    }

    @Get('/by-user-id/:userId')
    async getTodos(@Param('userId') userId: string) {
        try {
            const todos = await this.todoService.getAllTodos(userId);

            return todos;
        } catch (error) {
            throw new HttpException('Запрошенной задачи не существует', HttpStatus.NOT_FOUND);
        }
    }

}
